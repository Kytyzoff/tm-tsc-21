package ru.tsc.borisyuk.tm.command;

import ru.tsc.borisyuk.tm.exception.empty.EmptyNameException;
import ru.tsc.borisyuk.tm.exception.entity.TaskNotFoundException;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(Task task) {
        final List<Task> tasks = serviceLocator.getTaskService().findAll();
        final Integer indexNum = tasks.indexOf(task) + 1;
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Index: " + indexNum);
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project id: " + task.getProjectId());
    }

    protected Task add(final String name, final String description) {
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty()) throw new EmptyNameException();
        return new Task(name, description, new Date());
    }

}
