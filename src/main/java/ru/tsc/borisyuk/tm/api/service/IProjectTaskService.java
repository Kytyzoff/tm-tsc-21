package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskById(String userId, String projectId, String taskId);

    Task unbindTaskById(String userId, String projectId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

    Project removeById(String userId, String projectId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

}
