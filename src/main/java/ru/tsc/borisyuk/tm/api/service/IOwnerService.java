package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    void add(String userId, E entity);

    void remove(String userId, E entity);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    void clear(String userId);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    E removeById(String userId, String id);

    E removeByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    boolean existsByIndex(String userId, Integer index);

}
