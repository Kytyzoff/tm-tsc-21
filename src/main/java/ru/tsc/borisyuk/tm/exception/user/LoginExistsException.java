package ru.tsc.borisyuk.tm.exception.user;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
