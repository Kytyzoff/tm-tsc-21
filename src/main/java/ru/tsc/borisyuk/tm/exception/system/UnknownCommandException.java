package ru.tsc.borisyuk.tm.exception.system;

import ru.tsc.borisyuk.tm.constant.TerminalConst;
import ru.tsc.borisyuk.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Incorrect command. Use '" + TerminalConst.HELP + "' to display list of terminal commands.");
    }

}
