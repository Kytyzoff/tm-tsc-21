package ru.tsc.borisyuk.tm.repository;

import ru.tsc.borisyuk.tm.api.repository.IProjectRepository;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.model.Project;


public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public Project findByName(final String userId, final String name) {
        return entities.stream()
                .filter(p -> p.getId().equals(name))
                .filter(p -> p.getUserId().equals(userId))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        entities.remove(project);
        return project;
    }

    @Override
    public Project startById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(final String userId, final String name, final Status status) {
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
